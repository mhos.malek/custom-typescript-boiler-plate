import React from "react";
import "./App.css";
import RootController from "./providers";

function App() {
  return <RootController />;
}

export default App;
