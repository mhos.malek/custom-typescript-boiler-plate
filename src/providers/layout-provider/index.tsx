import React from "react";
import MainLayout from "../../layouts/main-layout";
import AuthLayout from "../../layouts/auth-layout";

import { LayoutProviderInterface, LAYOUT_TYPES } from "./model";

const LayoutProvider: React.FunctionComponent<LayoutProviderInterface> = ({
  layout,
  children,
}) => {
  return (
    <>
      {layout === LAYOUT_TYPES.MAIN ? (
        <MainLayout>{children}</MainLayout>
      ) : (
        <AuthLayout>{children}</AuthLayout>
      )}
    </>
  );
};
export default LayoutProvider;
