import React from "react";
import { AccessLevelProviderInterface } from "./model";

const AccessLevelProvider: React.FunctionComponent<AccessLevelProviderInterface> = ({
  children,
}) => {
  return <>{children}</>;
};

export default AccessLevelProvider;
