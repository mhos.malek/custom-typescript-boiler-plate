/* eslint-disable class-methods-use-this */
import { call, put } from "redux-saga/effects";
class ErrorProvider {
  *networkErrorHandler(error: any) {
    // send user error to sentry dashboard
    if (error.response) {
      // check what kind of error we have
      switch (error.response.status) {
        case 400:
          return { error };
        case 401:
          // yield put(unAuthenticateAction());
          yield window.localStorage.removeItem("token");

          break;
        case 403:
          break;
        case 429:
          break;
        default:
          return { error };
      }
    }
    return { error };
  }
}

export default new ErrorProvider();
