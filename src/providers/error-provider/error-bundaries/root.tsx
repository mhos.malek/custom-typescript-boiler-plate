import * as React from 'react';

interface RootErrorBoundaryState {
  hasError: boolean;
}
class RootErrorBoundary extends React.Component<{}, RootErrorBoundaryState> {
  constructor(props: { children: React.ReactNode }) {
    super(props);
    this.state = { hasError: false };
  }

  static getDerivedStateFromError() {
    // Update state so the next render will show the fallback UI.
    return { hasError: true };
  }

  componentDidCatch(error: Error) {
    // ErrorProvider.captureErrorWithSentry(error);
  }

  render() {
    const { hasError } = this.state;
    const { children } = this.props;
    if (hasError) {
      return <h1>s</h1>;
    }

    return children;
  }
}

export default RootErrorBoundary;
