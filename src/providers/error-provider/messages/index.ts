export const ERROR_REACT_BUNDARIES = {
  title: "خطا",
  message:
    "متاسفانه در اجرای اپلیکیشن ما خطایی به وجود آماده است. ما از وجود این مشکل به صورت خودکار آگاه می‌شویم و در حال پیگیری رفع آن هستیم!",
  type: "error",
};
