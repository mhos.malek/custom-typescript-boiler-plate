import paths from "./utils/paths";
import HomePage from "@/pages/home-page";
import NotFound from "@pages/not-found-page";
import LoginPage from "@pages/login-page";
import RegisterUserPage from "@pages/register-user-page"
const routes = [
  // rewards
  {
    path: paths.HOME,
    component: HomePage,
    isPrivate: true,
    exact: true,
  },
  {
    path: paths.REGISTER_USER,
    component: RegisterUserPage,
    isPrivate: true,
    exact: true,
  },

  {
    path: paths.LOGIN,
    component: LoginPage,
    isPrivate: false,
    exact: true,
  },


  // general routes
  {
    path: paths.NOT_FOUND,
    component: NotFound,
    isPrivate: false,
    exact: false,
  },
];

export default routes;
