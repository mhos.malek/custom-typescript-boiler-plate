import React from "react";
import { Redirect, Route } from "react-router-dom";
import pathes from "../utils/paths";

const PrivateRoute = ({ children, isAuthenticated, index, path, exact }) => {
  if (isAuthenticated)
    return (
      <Route key={index} path={path} exact={exact}>
        {children}
      </Route>
    );
  return <Redirect to={pathes.LOGIN} />;
};

export default PrivateRoute;
