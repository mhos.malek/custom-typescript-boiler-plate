import React from "react";
import { Router, Switch, Route } from "react-router-dom";
import routes from "./routes";
import { createBrowserHistory } from "history";
import { RouterProviderInterface } from "./model";
import { LAYOUT_TYPES } from "../layout-provider/model";
import PrivateRoute from "./private-route";
import LayoutProvider from "../layout-provider";

const BrowserHistory = createBrowserHistory();

const RouterProvider: React.FunctionComponent<RouterProviderInterface> = () => {
  return (
    <>
      <Router history={BrowserHistory}>
        <Switch>
          {routes.map((route, index) => {
            return route.isPrivate ? (
              <PrivateRoute
                isAuthenticated={true}
                index={index}
                path={route.path}
                exact={route.exact}
              >
                <LayoutProvider layout={LAYOUT_TYPES.MAIN}>
                  <route.component />
                </LayoutProvider>
              </PrivateRoute>
            ) : (
              <Route
                key={index}
                path={route.path}
                exact={route.exact}
                component={route.component}
              >
                <LayoutProvider layout={LAYOUT_TYPES.AUTH}>
                  <route.component />
                </LayoutProvider>
              </Route>
            );
          })}
        </Switch>
      </Router>
    </>
  );
};
export { BrowserHistory };
export default RouterProvider;
