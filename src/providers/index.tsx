// all controllers will be mounted here

import React from "react";
import AppProvider from "./app-provider";
import RouterProvider from "./router-provider";
import FontsProvider from "./fonts-provider";
import AccessLevelProvider from "./access-level-provider";

const RootController: React.FunctionComponent<{}> = () => {
  return (
    <AppProvider>
      <FontsProvider />
      <AccessLevelProvider>
        <RouterProvider />
      </AccessLevelProvider>
    </AppProvider>
  );
};

export default RootController;
