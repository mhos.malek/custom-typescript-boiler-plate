import React from "react";
import { AppProviderInterface } from "./model";

const AppProvider: React.FunctionComponent<AppProviderInterface> = ({
  children
}) => {
  return <div className="App">{children}</div>;
};

export default AppProvider;
