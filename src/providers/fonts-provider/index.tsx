import React from "react";
import { FontsProviderInterface } from "./model";
import GlobalFontInjector from "./style";

// if any font logic needed we add it in here:)
const FontsProvider: React.FunctionComponent<FontsProviderInterface> = ({
  fontFamilyName = "IranSans"
}) => {
  return <GlobalFontInjector fontFamily={fontFamilyName} />;
};

export default FontsProvider;
