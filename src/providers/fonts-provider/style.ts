import { createGlobalStyle } from "styled-components";
import "./assets/fonts/iransans/load.css";

const GlobalFontInjector = createGlobalStyle`
html, body {
    font-family: ${({ fontFamily }) => fontFamily};
  }

  span, p,h1,h2,h3,h4,h5,h6  {
    font-family: ${({ fontFamily }) => fontFamily};
  }
`;

export default GlobalFontInjector;
