import { ActionType, BootstrapApp } from "./action-type";

export const bootstrapApp = (): BootstrapApp => ({
  type: ActionType.APP_BOOTSTRAPPED,
});

export type AppAction = BootstrapApp;
