
export enum ActionType {
  APP_BOOTSTRAPPED = "APP_BOOTSTRAPPED",
}

export interface BootstrapApp {
  type: ActionType.APP_BOOTSTRAPPED;
}
