import { AppAction } from "./action";

export interface AppState {
  showSplash: boolean;
}

const appInitialState: AppState = {
  showSplash: true,
};

const reducer = (
  state: AppState = appInitialState,
  action: AppAction
): AppState => {
  switch (action.type) {
    default:
      return state;
  }
};
export default reducer;
