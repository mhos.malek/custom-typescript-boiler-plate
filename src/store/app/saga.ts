import { take } from "redux-saga/effects";

import { ActionType } from "./action-type";

function* appBootstrap() {
  yield take(ActionType.APP_BOOTSTRAPPED);
  /*
   ** Dispatch all actions needed when
   ** the app is loaded for the first time
   */
}

export default [appBootstrap()];
