import { call } from 'redux-saga/effects';
// import ErrorProvider from '@providers/error-provider';

export default function* callApiEndpoint(fn: any, ...rest: any[]) {
  try {
    const response = yield call(fn, ...rest);
    return response.data === null ? { status: 'ok' } : response.data;
  } catch (err) {
    // const error = yield ErrorProvider.networkErrorHandler(err);
    // return error;
  }
}
