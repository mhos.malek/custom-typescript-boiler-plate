const path = require('path');
const fs = require('fs');
const appDirectory = fs.realpathSync(process.cwd());
const resolveApp = relativePath => path.resolve(appDirectory, relativePath);

module.exports = {
  "@": resolveApp('src'),
  "@components": resolveApp('src/components'),
  "@store":  resolveApp('src/store'),
  "@pages":  resolveApp('src/pages'),
  "@providers": resolveApp('src/providers'),
  "@services": resolveApp('src/services'),
  "@utils": resolveApp('src/utils'),
};
